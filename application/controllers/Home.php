<?php

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		
	}
	
	public function index()
	{
		$this->load->view('login');
	}

	public function dashboard()
	{
		if (!$this->session->userdata('username')){
			return redirect('home/index');
		}
		$this->load->view('HalamanUtama');

	}
}

?>