<?php

class JenisObat extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Model_JenisObat');
        $this->load->library('form_validation');
    }
    
    public function index()
    {
        $data['JenisObat'] = $this->Model_JenisObat->getAllJenisObat();
        $this->load->view('template/header');
		
        $this->load->view('ManageJenis', $data);
        $this->load->view('template/footer');
    }

	public function pageAdd()
	{
		$this->load->view('template/header');
		$this->load->view('TambahJenis');
        $this->load->view('template/footer');
	}

    public function tambahData()
    {
        $this->Model_JenisObat->tambahData();
        redirect('JenisObat');
    }

    public function pageEdit($id_jenis_obat )
    {
        
        $data['JenisObat'] = $this->Model_JenisObat->getJenisObatById($id_jenis_obat);
        $this->load->view('template/header');
		$this->load->view('EditJenis', $data);
        $this->load->view('template/footer');
    }

    public function editData()
    {
        $this->Model_JenisObat->update();
        redirect('JenisObat');
    }

    public function delete($id_jenis_obat)
    {
        $this->Model_JenisObat->hapusData($id_jenis_obat);
        redirect('JenisObat');
    }
}

?>