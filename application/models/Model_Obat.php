<?php

class Model_Obat extends CI_Model{
    
   public function getAllObat()
   {
      return $this->db->get('tb_obat')->result_array();
   }

   public function tambahData()
   {
      $data = [
         "nama_obat" => $this->input->post('nama_obat', true),
         "id_jenis_obat" => $this->input->post('id_jenis_obat', true),
         "satuan" => $this->input->post('satuan', true),
         "harga" => $this->input->post('harga', true),
         "stok" => $this->input->post('stok', true),
         "tanggal_expired" => $this->input->post('tanggal_expired', true),
      ];
      $this->db->insert('tb_obat', $data);
   }

   public function getObatById($id_obat)
   {
      return $this->db->get_where('tb_obat', ['id_obat' => $id_obat])->row_array();
   }

   public function update()
   {
      $data = [
         "nama_obat" => $this->input->post('nama_obat', true),
         "satuan" => $this->input->post('satuan', true),
         "harga" => $this->input->post('harga', true),
         "stok" => $this->input->post('stok', true),
         "tanggal_expired" => $this->input->post('tanggal_expired', true),
      ];
      $this->db->where('id_obat', $this->input->post('id_obat'));
      $this->db->update('tb_obat', $data);
   }

   public function hapusData($id_obat)
   {
      $this->db->where('id_obat', $id_obat);
      $this->db->delete('tb_obat');
   }

}  