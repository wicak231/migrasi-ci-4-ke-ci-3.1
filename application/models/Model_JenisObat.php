<?php

class Model_JenisObat extends CI_Model{

   public function getAllJenisObat()
   {
      return $this->db->get('tb_jenis_obat')->result_array();
   }

   public function tambahData()
   {
      $data = [
         "nama_jenis_obat" => $this->input->post('nama_jenis_obat', true),
      ];
      $this->db->insert('tb_jenis_obat', $data);
   }

   public function getJenisObatById($id_jenis_obat)
   {
      return $this->db->get_where('tb_jenis_obat', ['id_jenis_obat' => $id_jenis_obat])->row_array();
   }

   public function update()
   {
      $data = [
         "nama_jenis_obat" => $this->input->post('nama_jenis_obat', true),
      ];
      $this->db->where('id_jenis_obat', $this->input->post('id_jenis_obat'));
      $this->db->update('tb_jenis_obat', $data);
   }

   public function hapusData($id_jenis_obat)
   {
      $this->db->where('id_jenis_obat', $id_jenis_obat);
      $this->db->delete('tb_jenis_obat');
   }
}