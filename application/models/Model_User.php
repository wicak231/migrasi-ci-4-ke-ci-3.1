<?php

class Model_User extends CI_Model{

   public function register()
   {
      $data = [
         "fullname" => $this->input->post('fullname', true),
         "username" => $this->input->post('username', true),
         "password" => $this->input->post('password', true),
         "level" => $this->input->post('level', true),
         "is_active" => $this->input->post('is_active', true),
      ];
      $this->db->insert('tb_user', $data);
   }
   
   public function getAllUser()
   {
      return $this->db->get('tb_user')->result_array();
   }

   public function getUserById($id_user)
   {
      return $this->db->get_where('tb_user', ['id_user' => $id_user])->row_array();
   }

   public function update()
   {
      $data = [
         "fullname" => $this->input->post('fullname', true),
         "username" => $this->input->post('username', true),
         "is_active" => $this->input->post('is_active', true),
      ];
      $this->db->where('id_user', $this->input->post('id_user'));
      $this->db->update('tb_user', $data);
   }

   public function hapusData($id_user)
   {
      $this->db->where('id_user', $id_user);
      $this->db->delete('tb_user');
   }

   public function getUser($username)
   {
      return $this->db->get_where('tb_user', ['username' => $username])->row_array();
   }
   
}