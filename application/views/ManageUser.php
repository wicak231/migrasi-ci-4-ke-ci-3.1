

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Manajemen Data User</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <div>
      
      <a href="addObat"><button class="btn btn-success">Tambah User</button></a>
<table class="table mt-4">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">ID User</th>
      <th scope="col">Fullname</th>
      <th scope="col">Username</th>
      <th scope="col">Status</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
  <?php
    $no = 0;
    foreach ($User as $item):
    $no++;
    ?>
    <tr>
      <td><?= $no ?></td>
      <td><?= $item['id_user']; ?></td>
      <td><?= $item['fullname']; ?></td>
      <td><?= $item['username']; ?></td>
      <td><?= $item['is_active']; ?></td>
      <td>
      <a href="<?= base_url('User/pageEdit/'.$item['id_user']) ?>"><button class="btn btn-primary">Edit</button></a>
      <a href="<?= base_url('User/delete/'.$item['id_user']) ?>"><button class="btn btn-danger">Delete</button></a>
      </td>
    </tr>
    <?php
    endforeach;
    ?>
  </tbody>
</table>

      </div>
       
      </div>
    </section>
  </div>


