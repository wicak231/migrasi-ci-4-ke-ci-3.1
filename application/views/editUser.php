
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Edit Data Obat</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <div>
      <form method="post" action="<?= base_url('User/editData/') ?>"> 
      <input type="hidden" name="id_user" value="<?= $User['id_user']; ?>">
        <div class="mb-3">
            <label class="form-label">Fullname</label>
            <input type="text" class="form-control" id="nama" name="fullname" 
            value="<?= $User['fullname']; ?>">
            <label class="form-label">Username</label>
            <input type="text" class="form-control" id="nama" name="username" 
            value="<?= $User['username']; ?>">
            <label class="form-label">Status</label>
            <select class="form-control form-select-lg mb-3" aria-label=".form-select-lg example" name="is_active">
            <option selected><?= $User['is_active']; ?></option>
            <option value="on">Aktifkan</option>
            <option value="off">Noaktifkan</option>
            </select>
        <button type="submit" class="btn btn-primary mt-4">Simpan</button>
        </form>
      
      </div>
       
      </div>
    </section>
  </div>


