


  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
        <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-primary">
              <div class="inner">
              <b>Data Obat</b>  
              <h3>0</h3>
              <p>.</p>  
              <p>.</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
              <b>Data Obat</b>  
              <h3>0</h3>
              <p>Jumlah Obat Belum Expired :</p>  
              <p>Jumlah Obat Sudah Expired :</p>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-4 col-6">
          <div class="small-box bg-warning">
              <div class="inner">
              <b>Data User</b>  
              <h3>0</h3>
              <p>Jumlah User Aktif : </p>  
              <p>Jumlah User No Aktif :</p>
               
              </div>
              <div class="icon">
                <i class="ion ion-person-add"></i>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
            </div>
        </div>
        <hr>
        <div>
          <nav class="navbar navbar-light bg-light">
            <div class="container-fluid">
            <select class="btn form-select-lg mb-3" aria-label=".form-select-lg example" name="satuan">
              <option selected>-- Pilih Satuan -- </option>
              <option value="mg">mg</option>
              <option value="ml">ml</option>
            </select>
            <form class="d-flex">
                <input class="form-control me-2" type="date" >
                <p></p>
                <input class="form-control me-2" type="date" >
                <button class="btn btn-outline-success" type="submit">Search</button>
              </form>
              <form class="d-flex">
                <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success" type="submit">Search</button>
              </form>
            </div>
          </nav>
        </div>
        <table class="table mt-4">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama Obat</th>
      <th scope="col">Satuan</th>
      <th scope="col">Harga</th>
      <th scope="col">Stok</th>
      <th scope="col">Jumlah Harga</th>
      <th scope="col">Tanggal Expired</th>
      
    </tr>
  </thead>
  <tbody>
    
    <tr>
      <td>1</td>
      <td>Test Data</td>
      <td>Test Data</td>
      <td>Test Data</td>
      <td>Test Data</td>
      <td>Test Data</td>
      <td>Test Data</td>
      </td>
    </tr>
    <tr>
      <td>1</td>
      <td>Test Data</td>
      <td>Test Data</td>
      <td>Test Data</td>
      <td>Test Data</td>
      <td>Test Data</td>
      <td>Test Data</td>
      </td>
    </tr>
    <tr>
      <td>1</td>
      <td>Test Data</td>
      <td>Test Data</td>
      <td>Test Data</td>
      <td>Test Data</td>
      <td>Test Data</td>
      <td>Test Data</td>
      </td>
    </tr>
    <tr>
      <td>1</td>
      <td>Test Data</td>
      <td>Test Data</td>
      <td>Test Data</td>
      <td>Test Data</td>
      <td>Test Data</td>
      <td>Test Data</td>
      </td>
    </tr>
  </tbody>
</table>

<div>
<div class="btn-group" role="group" aria-label="Basic mixed styles example">
  <button type="button" class="btn btn-warning">Cetak PDF</button>
  <button type="button" class="btn btn-success">Cetak Excel</button>
</div>
</div>
      </div>
    </section>
  </div>

